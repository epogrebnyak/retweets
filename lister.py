from typing import Iterable
from pathlib import Path




def shortcode(num: int):
    return "{{< tweet " + str(num) + " >}}"


def joins(xs: Iterable[str]):
    return "\n\n".join(xs)



import yaml

def yaml_load(content):
    return yaml.load(content, Loader=yaml.FullLoader)

def group_items(group):
    before = group.get("before")
    if before:
        yield before
    for tw in group["tweets"]:
        yield shortcode(tw)

def group_to_markdown(group):
    return joins(group_items(group))


def md_index_text(folder, body):
    title = str(folder.name).capitalize()
    return f"""---
bookFlatSection: true
title: "{title}"
---

{body}
"""

1350500299882991616

doc = """
tools:
- filename: productivity.md
  menu: Productivity
  title: Productivity
  groups:
    - before: "## Mindmaps"
      tweets: 
      - 1343440088894726144
  groups:
    - before: "## Todo lists"
      tweets: 
      - 1350500299882991616
visuals:
- filename: maps.md
  menu: Maps
  title: Maps
  tagline: Some maps are more true than others
  groups:
    - before: "## Germany"
      tweets: 
      - 1349728712221048837
      - 1348852931672629250    
software:
- filename: architecture.md
  menu: Architecture
  title: Software architecture  
  groups:
    - before: "## The modern stack"
      tweets: 
      - 1341413011668553730  
    - before: "## An AWS story"
      tweets: 
      - 1347677573900242944
- filename: design_patterns.md
  menu: Design patterns
  title: Design patterns  
  groups:
    - before: "## GOF vs functional"
      tweets: 
      - 912561931243974659
      - 1350864307404337156
    - before: "## Other"
      tweets: 
      - 1348895595667320834      
- filename: laws.md
  menu: Laws
  title: Laws
  groups:
    - tweets: []
      before: |
            It is very appealling to organise some activity according to a set laws and axioms 
            that should govern where the truth is. Software craftsmanship is a complex business 
            that does need proper organisation, so "laws" emerge often. 
            
            These laws are usually the empiric rules derived from trial and error. 
            Some of these laws can hit you on a head immediately - like gravity - 
            and some laws you can debate about for a while until they apply.
            
            You may find an extensive list [here](https://github.com/dwmkerr/hacker-laws), 
            but also check some retweets below.        
    - before: |
          ## Brooks: silver bullet     
          
          There is no silver bullet (perfect solution), but there is 
          always a through-away (first prototype).
      tweets: 
      - 869988355420831745
      - 1331766297030438913
    - before: "## Conway: people"
      tweets: 
      - 1350182362550661120
      - 1204817799580921856
      - 1291581726058143744
    - before: "## Gull: architecture"
      tweets: 
      - 1348702592776372224


- filename: teaching.md
  menu: Teaching
  title: Teaching
  groups:
    - tweets: 
      - 1347488424551473153

- filename: reliability.md
  menu: Reliability
  title: Reliability
  groups:
    - tweets: 
      - 1350182362550661120
      - 1204817799580921856
      - 1291581726058143744
economics:
- filename: reading_list.md
  menu: Reading list
  title: Economics reading list
  groups:
    - before: "Mostly long-reads"
      tweets: 
      - 1340196262109392896
      - 1337696859410853900
      - 1336041287720579073  
- filename: teaching.md
  menu: Better teaching
  title: Teaching economics better
  groups:
    - tweets: 
      - 1347607709126848513
"""

def write_index_md(folder, text=""):
    content = md_index_text(folder, text)
    (folder / "_index.md").write_text(content)
    return content 
    
def front_matter(file):    
    title = file.get("menu") if file.get("menu") else file.get("title")
    return f"""---
bookFlatSection: true
title: "{title}"
---""" + "\n"

def body_items(file):
    yield f"# {file.get('title')}"
    for group in file["groups"]:
        for item in group_items(group):
            yield item

def md_text(file):    
    return  front_matter(file) + joins(body_items(file))


destination_folder = "content/docs2"
root = Path(destination_folder)
write_index_md(root)
source = yaml_load(doc).items()
for folder, files in source:
    cur_folder = root / folder
    try:
        cur_folder.mkdir(parents=True)
    except FileExistsError:
        pass
    write_index_md(cur_folder)
    for file in files:
        path = cur_folder / file["filename"]
        path.write_text(md_text(file))
        print(path)
        print(path.read_text())