---
weight: 2
bookFlatSection: true
title: "Laws in Software Engineering"
---

# Laws in Software Engineering

It is very appealling to organise some activity according to a set laws and axioms 
that should govern where the truth is. Software craftsmanship is a complex business 
that does need proper organisation, so "laws" emerge often. 

These laws are usually the empiric rules derived from trial and error. 
Some of these laws can hit you on a head immediately - like gravity - 
and some laws you can debate about for a while until they apply.

You may find an extensive list [here](https://github.com/dwmkerr/hacker-laws), 
but also check some retweets below.

## Brooks: silver bullet

- No silver bullet.
- There is always a through-away.

<center>

{{< tweet 1331766297030438913 >}}

</center>

## Conway: people

H/T: @WolfgangBremer, @mfeathers, @CatSwetel

<center>

{{< tweet 1350182362550661120 >}}

{{< tweet 1204817799580921856 >}}

{{< tweet 1291581726058143744 >}}

</center>

## Gull: architecture

H/T: @allenholub

<center>

{{< tweet 1348702592776372224 >}}

</center>

## Other

- [Mental models](https://twitter.com/george__mack/status/1350513143387189248)
