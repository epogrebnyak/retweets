---
bookFlatSection: true
title: "Laws"
---
# Laws

It is very appealling to organise some activity according to a set laws and axioms 
that should govern where the truth is. Software craftsmanship is a complex business 
that does need proper organisation, so "laws" emerge often. 

These laws are usually the empiric rules derived from trial and error. 
Some of these laws can hit you on a head immediately - like gravity - 
and some laws you can debate about for a while until they apply.

You may find an extensive list [here](https://github.com/dwmkerr/hacker-laws), 
but also check some retweets below.        


## Brooks: silver bullet     

There is no silver bullet (perfect solution), but there is 
always a through-away (first prototype).


{{< tweet 869988355420831745 >}}

{{< tweet 1331766297030438913 >}}

## Conway: people

{{< tweet 1350182362550661120 >}}

{{< tweet 1204817799580921856 >}}

{{< tweet 1291581726058143744 >}}

## Gull: architecture

{{< tweet 1348702592776372224 >}}