---
title: Introduction
type: docs
---

# Clustering tweets

Other that settling on just one theme
this site intends to use Hugo's twitter
shortcode to assemble collection of tweets
that I wanted to revisit in group.

<center>
{{< tweet 1229285967326072832 >}}
</center>


# End of search of a better theme

This site is an attempt to break from
"there is a better theme and layout"
curse as shown below:

<center>
{{< tweet 1350763021266800640 >}}
</center>

Of course there is, and some already
it doing more beautiful and meaningful blogging
that you do. I'm about to accept this.
