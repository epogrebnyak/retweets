serve:
    hugo server --disableFastRender

clean:
    rm -rf public resources

pretty:
    npx prettier -w content

publish:
    git add . && git commit -m"update" && git push

echo:         
    echo 
    


#   "scripts": {
# "init": "rimraf .git && git init -b main",
# "create": "hugo new",
# "prestart": "npm run clean",
# "start": "hugo server --disableFastRender",
# "prebuild": "npm run clean",
# "build": "hugo --gc --minify && npm run build:functions",
# "build:functions": "netlify-lambda build assets/lambda",
# "build:preview": "npm run build -D -F",
# "clean": "rimraf public resources functions",
# "lint": "npm run -s lint:scripts && npm run -s lint:styles && npm run -s lint:markdown",
# "lint:scripts": "eslint assets/js assets/lambda config",
# "lint:styles": "stylelint \"assets/scss/**/*.{css,sass,scss,sss,less}\"",
# "lint:markdown": "markdownlint *.md content/**/*.md",
# "release": "standard-version",
# "server": "hugo server",
# "test": "npm run -s lint",
# "env": "env"
